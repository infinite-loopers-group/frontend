# Utilisation de l'image Node.js 16 comme base
FROM node:16

# Création du répertoire de travail dans l'image
WORKDIR /app

# Copie des fichiers package.json et yarn.lock dans le répertoire de travail
COPY package.json yarn.lock ./

# Installation des dépendances avec Yarn
RUN yarn install

# Copie de tous les fichiers du projet dans le répertoire de travail de l'image
COPY . .

#ENV NEXT_PUBLIC_BACKEND_URL https://morningnews-backend-preprod.lacapsuleproject.online
ENV $(cat .env | grep -v '^#' | xargs)

# Construction de l'application Next.js
RUN yarn build

# Exposition du port 3000 utilisé par Next.js (à ajuster si nécessaire)
EXPOSE 3000

# Commande pour démarrer l'application Next.js
CMD ["yarn", "start"]
